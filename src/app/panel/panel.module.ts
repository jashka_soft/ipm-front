import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

import { SettingsComponent } from "./components/account/settings/settings.component";
import { SettingsService } from "./components/account/settings/settings.service";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { DashboardMenuComponent } from "./components/menu/menu.component";
import { NavigationComponent } from "./components/dashboard/navigation/navigation.component";
import { PracticeComponent } from "./components/practice/practices.component";
import { PracticesService } from "./components/practice/practices.service";
import { PracticePageComponent } from "./components/practice/practice-page/practice-page.component";
import { PanelComponent } from "./components/panel/panel.component";
import { InfoComponent } from "./components/info/info.component";
import { InfoService } from './components/info/info.service';

import { environment } from "../../environments/environment";
import { routes } from "./routes";

import {
  AuthModule,
  InterceptorMessageModule,
  ITPMModule,
  NamedRouteModule,
  PaginationModule
} from "itpm-shared";
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    ReactiveFormsModule,

    LoadingBarHttpClientModule,

    AuthModule,
    PaginationModule.forRoot(environment),
    ITPMModule.forRoot(environment),
    InterceptorMessageModule,
    NamedRouteModule,

    SharedModule,
  ],
  providers: [
    SettingsService,
    PracticesService,
    InfoService,
  ],
  declarations: [
    PanelComponent,
    InfoComponent,
    DashboardComponent,
    DashboardMenuComponent,
    NavigationComponent,

    PracticeComponent,
    PracticePageComponent,

    SettingsComponent,
  ],
  exports: [],
})
export class PanelModule {
}
