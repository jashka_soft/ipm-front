import { Routes } from "@angular/router";

import { DiagramNonSavedGuard } from 'itpm-shared';

import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { InfoComponent } from "./components/info/info.component";
import { PracticeComponent } from "./components/practice/practices.component";
import { PracticePageComponent } from "./components/practice/practice-page/practice-page.component";
import { SettingsComponent } from "./components/account/settings/settings.component";

export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      routeName: 'panel'
    },
    children: [
      {
        path: '',
        component: InfoComponent,
        data: {
          routeName: 'panel'
        }
      },
      {
        path: 'courses',
        loadChildren: './modules/courses/courses.module#CoursesModule',
        data: {
          routeName: 'courses'
        }
      },
      {
        path: 'definitions',
        loadChildren: './modules/definitions/definitions.module#DefinitionsModule',
        data: {
          routeName: 'definitions'
        }
      },
      {
        path: 'practice',
        component: PracticeComponent,
        data: {
          routeName: 'practice'
        }
      },
      {
        path: 'practice/:id',
        component: PracticePageComponent,
        data: {
          routeName: 'practicePage'
        },
        canDeactivate: [ DiagramNonSavedGuard ]
      },
      {
        path: 'account/settings',
        component: SettingsComponent,
        data: {
          routeName: 'accountSettings'
        }
      },
      {
        path: 'statistics',
        loadChildren: './modules/statistics-wrap.module#StatisticsWrapModule'
      }
    ]
  }
];
