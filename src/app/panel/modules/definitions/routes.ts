import { Route } from "@angular/router";

import { DefinitionsComponent } from "./components/definitions/definitions.component";
import { DefinitionsMainComponent } from './components/definitions-main.component';

export const routes: Route[] = [
  {
    path: '',
    component: DefinitionsMainComponent,
    children: [
      {
        path: '',
        component: DefinitionsComponent,
        data: {
          routeName: 'definitions'
        }
      }
    ]
  },
];