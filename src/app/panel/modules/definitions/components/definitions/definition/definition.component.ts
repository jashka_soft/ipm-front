import { Component, Input } from "@angular/core";

import { Definition } from 'itpm-shared';

@Component({
  selector: 'definition',
  templateUrl: './definition.component.html'
})
export class DefinitionComponent {
  @Input() definition: Definition;
}