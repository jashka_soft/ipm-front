import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";

import { AppHttpClient, Definition, IPagination } from "itpm-shared";

@Injectable()
export class DefinitionsService {
  constructor(private AppHttpClient: AppHttpClient) {}

  public get(): Observable<IPagination<Definition>> {
    return this.AppHttpClient
      .get<IPagination<Definition>>('definitions/');
  }
}