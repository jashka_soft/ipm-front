import { Component } from "@angular/core";

import { DefinitionsService } from "./definitions.service";
import { PaginationService, Definition, IPagination } from "itpm-shared";

@Component({
  selector: 'definitions',
  templateUrl: './definitions.component.html'
})
export class DefinitionsComponent {
  constructor(private DefinitionsService: DefinitionsService,
              public definitions: PaginationService<Definition>) {
    this.DefinitionsService.get().subscribe((pagination: IPagination<Definition>) => {
      this.definitions.update(pagination);
    });
  }
}