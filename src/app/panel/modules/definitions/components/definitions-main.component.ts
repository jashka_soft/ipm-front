import { Component } from '@angular/core';

@Component({
  template: `
      <ngx-loading-bar></ngx-loading-bar>
      <router-outlet></router-outlet>`
})
export class DefinitionsMainComponent {

}