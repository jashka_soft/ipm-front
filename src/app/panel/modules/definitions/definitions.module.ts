import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

import { ITPMModule, PaginationModule } from "itpm-shared";
import { environment } from "../../../../environments/environment";
import { DefinitionsService } from "./components/definitions/definitions.service";
import { DefinitionsComponent } from "./components/definitions/definitions.component";
import { DefinitionComponent } from "./components/definitions/definition/definition.component";
import { routes } from "./routes";
import { DefinitionsMainComponent } from './components/definitions-main.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

    LoadingBarHttpClientModule,

    PaginationModule.forRoot(environment),
    ITPMModule.forRoot(environment),
  ],
  declarations: [
    DefinitionsMainComponent,
    DefinitionsComponent,
    DefinitionComponent,
  ],
  providers: [
    DefinitionsService
  ]
})
export class DefinitionsModule {

}