import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StatisticModule } from 'itpm-shared';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'itpm-shared#StatisticModule',
    data: {
      routeName: 'dashboard'
    },
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),

    StatisticModule,
  ],
})
export class StatisticsWrapModule {}