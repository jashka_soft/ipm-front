import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { Lesson } from "itpm-shared";

@Component({
  selector: 'lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.css']
})
export class LessonComponent implements OnInit {
  @Input() lesson: Lesson;

  public routeConfig: any = {};

  constructor(private ActivatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.routeConfig['id'] = +this.ActivatedRoute.snapshot.params['id'];
    this.routeConfig['lessonId'] = this.lesson.id;
  }
}
