import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/operator/map";

import { AppHttpClient, IPagination, Lesson } from "itpm-shared";

@Injectable()
export class LessonService {
  private prefix: string;

  constructor(private HttpClient: AppHttpClient) {
    this.prefix = 'lessons/';
  }

  public getLessons(courseId: number): Observable<IPagination<Lesson>> {
    return this.HttpClient.get<IPagination<Lesson>>(`${this.prefix}${courseId}`);
  }
}