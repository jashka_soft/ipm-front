import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IPagination, Lesson, PaginationService } from "itpm-shared";
import { LessonService } from "./lesson/lesson.service";

@Component({
  selector: 'lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.css'],
  providers: [
    PaginationService
  ]
})
export class LessonsComponent {
  constructor(public lessons: PaginationService<Lesson>,
              private LessonService: LessonService,
              private ActivatedRoute: ActivatedRoute) {
    const courseId: number = +this.ActivatedRoute.snapshot.params['id'];
    this.LessonService.getLessons(courseId).subscribe((pagination: IPagination<Lesson>) => {
      this.lessons.update(pagination);
    });
  }
}
