import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Unit } from "itpm-shared";
import { Examination } from "../../../../../examination/examination/examination";
import { UnitService } from './unit.service';
import { EX_DATA } from '../../../../../examination/examination/examination.service';

class CustomUnit extends Unit {
  examinations: Array<Examination>;
  current_examination: Examination;
}

@Component({
  selector: 'unit',
  templateUrl: './unit.component.html',
  styleUrls: ['./unit.component.css']
})
export class UnitComponent implements OnInit {
  @Input() unit: CustomUnit;

  public countExaminations: Array<number> = [];
  public text: string = 'Пройти';

  constructor(private Router: Router,
              private UnitService: UnitService) {
    this.countExaminations = Array.from({length: 3}, (v, k) => k + 1);
  }

  public ngOnInit() {
    this.text = this.unit.current_examination ? 'Продолжить' : 'Пройти';
  }

  public go() {
    if (this.unit.current_examination) {
      this.redirectToExamination(this.unit.current_examination.uuid);
    } else {
      this.UnitService.makeExamination(this.unit.id).subscribe((data: EX_DATA) => {
        this.unit.current_examination = data.examination;
        this.redirectToExamination(this.unit.current_examination.uuid);
      });
    }
  }

  private redirectToExamination(uuid: string) {
    this.Router.navigate([`/examination/${uuid}`]);
  }

}
