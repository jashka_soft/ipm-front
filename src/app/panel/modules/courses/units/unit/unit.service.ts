import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/operator/map';

import { AppHttpClient, IPagination, Unit } from 'itpm-shared';
import { EX_DATA } from '../../../../../examination/examination/examination.service';

@Injectable()
export class UnitService {
  private prefix: string;

  constructor(private HttpClient: AppHttpClient) {
    this.prefix = 'units/';
  }

  public getUnits(lessonId: number): Observable<IPagination<Unit>> {
    return this.HttpClient.get<IPagination<Unit>>(`${this.prefix}${lessonId}`);
  }

  public makeExamination(unitId: number): Observable<EX_DATA> {
    return this.HttpClient
      .post<EX_DATA>(`examination/`, {
        unit_id: unitId
      });
  }
}