import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { IPagination, PaginationService, Unit } from "itpm-shared";
import { UnitService } from "./unit/unit.service";
import { CountdownServerService } from '../../../../shared/countdown-server.service';

@Component({
  selector: 'units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css'],
  providers: [
    PaginationService
  ]
})
export class UnitsComponent implements OnInit {
  constructor(public units: PaginationService<Unit>,
              private UnitService: UnitService,
              private ActivatedRoute: ActivatedRoute,
              private CountdownServerService: CountdownServerService) {
    this.CountdownServerService.removeIfExpired().subscribe(() => {});
  }

  public ngOnInit () {
    const lessonId: number = +this.ActivatedRoute.snapshot.params['lessonId'];
    this.UnitService.getUnits(lessonId).subscribe((pagination: IPagination<Unit>) => {
      this.units.update(pagination);
    });
  }
}
