import { Injectable } from "@angular/core";

import "rxjs/operator/map";
import { Observable } from "rxjs/Observable";

import { AppHttpClient, Course, IPagination } from "itpm-shared";

@Injectable()
export class CourseService {
  private prefix: string;

  constructor(private AppHttpClient: AppHttpClient) {
    this.prefix = 'courses/';
  }

  getCourses(): Observable<IPagination<Course>> {
    return this.AppHttpClient.get<IPagination<Course>>(`${this.prefix}`);
  }
}