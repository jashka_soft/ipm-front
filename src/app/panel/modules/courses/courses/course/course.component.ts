import { Component, Input } from "@angular/core";

import { Course } from "itpm-shared";

@Component({
  selector: 'course-item',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
})
export class CourseComponent {

  @Input() course: Course;
}
