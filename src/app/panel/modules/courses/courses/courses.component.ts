import { Component, OnInit } from "@angular/core";

import { CourseService } from "./course/course.service";
import { Course, IPagination, PaginationService } from "itpm-shared";

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
  providers: [
    PaginationService
  ]
})
export class CoursesComponent implements OnInit {

  constructor(private CourseService: CourseService,
              public courses: PaginationService<Course>) {
  }

  public ngOnInit () {
    this.CourseService.getCourses().subscribe((pagination: IPagination<Course>) => {
      this.courses.update(pagination);
    });
  }
}
