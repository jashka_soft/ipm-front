import { Routes } from "@angular/router";

import { CoursesComponent } from "./courses/courses.component";
import { LessonsComponent } from "./lessons/lessons.component";
import { UnitsComponent } from "./units/units.component";
import { CoursesMainComponent } from './courses-main/courses-main.component';

export const routes: Routes = [
  {
    path: '',
    component: CoursesMainComponent,
    children: [
      {
        path: '',
        component: CoursesComponent,
        data: {
          routeName: 'courses'
        },
      },
      {
        path: ':id/lessons',
        component: LessonsComponent,
        data: {
          routeName: 'lessons'
        },
      },
      {
        path: ':id/lessons/:lessonId/units',
        component: UnitsComponent,
        data: {
          routeName: 'units'
        },
      }
    ]
  },
];
