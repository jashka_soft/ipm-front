import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

import { PaginationModule, NamedRouteModule, ITPMModule } from "itpm-shared";

import { CoursesComponent } from "./courses/courses.component";
import { CourseService } from "./courses/course/course.service";
import { CourseComponent } from "./courses/course/course.component";
import { LessonsComponent } from "./lessons/lessons.component";
import { LessonService } from "./lessons/lesson/lesson.service";
import { LessonComponent } from "./lessons/lesson/lesson.component";
import { UnitComponent } from "./units/unit/unit.component";
import { UnitService } from "./units/unit/unit.service";
import { UnitsComponent } from "./units/units.component";

import { environment } from "../../../../environments/environment";
import { routes } from "./routes";
import { CoursesMainComponent } from './courses-main/courses-main.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

    LoadingBarHttpClientModule,

    ITPMModule.forRoot(environment),
    NamedRouteModule,
    PaginationModule.forRoot(environment)
  ],
  declarations: [
    CoursesMainComponent,
    CourseComponent,
    CoursesComponent,

    LessonComponent,
    LessonsComponent,

    UnitComponent,
    UnitsComponent,
  ],
  providers: [
    CourseService,
    LessonService,
    UnitService,
  ],
})
export class CoursesModule {}