import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { AuthService } from "itpm-shared";

@Component({
  selector: 'dashboard-menu',
  templateUrl: `./menu.component.html`
})
export class DashboardMenuComponent {

  constructor(private AuthService: AuthService,
              private Router: Router) {
  }

  public logout() {
    this.AuthService.logout();
    this.Router.navigate(['/']);
  }
}