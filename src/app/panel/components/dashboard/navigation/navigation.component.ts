import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { AuthService } from "itpm-shared";

@Component({
  selector: `dashboard-navigation`,
  templateUrl: `./navigation.component.html`,
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  constructor(private AuthService: AuthService,
              private Router: Router) {
  }

  public logout() {
    this.AuthService.logout();
    this.Router.navigate(['/']);
  }
}