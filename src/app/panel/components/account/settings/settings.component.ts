import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SettingsService } from "./settings.service";

import { JWTService } from "itpm-shared";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  public Account: FormGroup;

  constructor(private fb: FormBuilder,
              private SettingsService: SettingsService,
              private JWTService: JWTService) {
  }

  public ngOnInit() {
    this.Account = this.fb.group({
      'password': [null, Validators.compose([
        Validators.required,
        Validators.minLength(8)
      ])],
      'password_confirmation': [null, Validators.compose([
        Validators.required,
        Validators.minLength(8)
      ])]
    });
  }

  public update(data) {
    this.SettingsService.update(data).subscribe(data => {
      this.JWTService.setToken(data.token);
    });
  }

}
