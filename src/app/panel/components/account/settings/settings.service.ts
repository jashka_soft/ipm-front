import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";

import { AppHttpClient } from "itpm-shared";

@Injectable()
export class SettingsService {
  constructor(private HttpClient: AppHttpClient) {
  }

  public update(data): Observable<any> {
    return this.HttpClient.post<any>(`account`, data);
  }
}