import { Component } from "@angular/core";

import { InfoService } from './info.service';
import { Examination } from '../../../examination/examination/examination';
import { CountdownServerService } from '../../../shared/countdown-server.service';

@Component({
  selector: 'info',
  templateUrl: './info.component.html',
})
export class InfoComponent {
  public title: string = 'info of user';
  public currentExaminations: Array<Examination> = [];

  public constructor(private InfoService: InfoService,
                     private CountdownServerService: CountdownServerService) {
    this.CountdownServerService.removeIfExpired().subscribe(() => {
      this.InfoService.getCurrentExaminations().subscribe((examinations: Array<Examination>) => {
        this.currentExaminations = examinations;
      });
    });
  }
}