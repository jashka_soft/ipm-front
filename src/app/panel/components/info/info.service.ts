import { Injectable } from '@angular/core';

import { AppHttpClient } from 'itpm-shared';
import { Observable } from 'rxjs/Observable';
import { Examination } from '../../../examination/examination/examination';

@Injectable()
export class InfoService {
  public constructor(private HttpClient: AppHttpClient) {}

  public getCurrentExaminations(): Observable<Array<Examination>> {
    return this.HttpClient.get<Array<Examination>>(`current`);
  }
}