import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {
  AuthService, DiagramInstanceConfig, DiagramSavedService, instances, Permissions, Schema,
  SchemaService
} from 'itpm-shared';

@Component({
  selector: 'practice-page',
  templateUrl: './practice-page.component.html'
})
export class PracticePageComponent implements OnInit, OnDestroy {

  public schema: Schema = null;
  public config: any = {};

  private isSaved: boolean = false;
  private instances: DiagramInstanceConfig[] = [];

  constructor(private ActivatedRoute: ActivatedRoute,
              private SchemaService: SchemaService,
              private AuthService: AuthService,
              private DiagramSavedService: DiagramSavedService) {
    this.config['role'] = this.AuthService.getUser().role;
    this.config['permission'] = Permissions.FULL_ACCESS;
    this.config['controls'] = true;
    this.instances = instances;
  }

  @HostListener('window:beforeunload', ['$event'])
  public onUnloadWindow($event) {
    return !this.isSaved ? $event.returnValue = true : null;
  }

  public ngOnInit() {
    let idOrNew = this.ActivatedRoute.snapshot.paramMap.get('id');
    if (idOrNew === 'new') {
      this.schema = new Schema({
        type: 'WBSOBS',
        unit_id: null,
        name: 'schema name'
      });
    } else {
      this.SchemaService.get(+idOrNew).subscribe((schema: Schema) => {
        this.schema = schema;
      });
    }
  }

  public ngOnDestroy() {

  }

  public onUpdateDiagram(schema: Schema) {
    this.isSaved = false;
  }

  public save(schema: Schema) {
    // this.schema.name = schema.name;
    this.schema.diagram = schema.diagram;
    this.SchemaService.store(this.schema).subscribe((schema: Schema) => {
      this.schema = schema;
      this.isSaved = true;
      this.DiagramSavedService.save();
    });
  }

}