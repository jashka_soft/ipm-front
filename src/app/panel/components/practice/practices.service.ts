import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";

import { Schema, AppHttpClient, IPagination } from "itpm-shared";

@Injectable()
export class PracticesService {
  private prefix: string;

  constructor(private AppHttpClient: AppHttpClient) {
    this.prefix = 'schema/';
  }

  public getSchemas(): Observable<IPagination<Schema>> {
    return this.AppHttpClient
      .withoutPrefixUrl()
      .get<IPagination<Schema>>(`${this.prefix}`);
  }
}