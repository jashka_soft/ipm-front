import { Component } from "@angular/core";

import { PracticesService } from "./practices.service";

import {
  PaginationService,
  IPagination,
  SchemaService,
  Schema
} from "itpm-shared";

@Component({
  selector: 'practices',
  templateUrl: './practices.component.html'
})
export class PracticeComponent {

  constructor(private PracticesService: PracticesService,
              private SchemaService: SchemaService,
              public schemas: PaginationService<Schema>) {
    this.PracticesService.getSchemas().subscribe((pagination: IPagination<Schema>) => {
      this.schemas.update(pagination);
    });
  }

  public remove(id: number) {
    this.SchemaService.delete(id).subscribe(() => {
      this.schemas.removeFromCollection(id);
    });
  }
}