import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { ToastModule } from "ng2-toastr/ng2-toastr";
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

import { AppComponent } from "./app.component";

import { AuthenticationComponent } from "./auth/authentication.component";
import { SignUpComponent } from "./auth/signup/signup.component";
import { ConfirmationComponent } from "./auth/confirmation/confirmation.component";
import { ConfirmationService } from "./auth/confirmation/confirmation.service";
import {
  AuthModule,
  InterceptorMessageModule,
  ITPMModule,
  NamedRouteModule
} from "itpm-shared";
import { environment } from "../environments/environment";
import { routes } from "./routes";

@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    SignUpComponent,
    ConfirmationComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule,

    LoadingBarHttpClientModule,

    AuthModule,
    NamedRouteModule,
    ITPMModule.forRoot(environment),
    InterceptorMessageModule
  ],
  providers: [
    ConfirmationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
