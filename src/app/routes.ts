import { Routes } from "@angular/router";

import { AuthenticationComponent } from "./auth/authentication.component";
import { SignUpComponent } from "./auth/signup/signup.component";
import { ConfirmationComponent } from "./auth/confirmation/confirmation.component";
import { AuthGuard } from "itpm-shared";

export const routes: Routes = [
  {
    // root route
    path: '',
    pathMatch: 'full',
    component: AuthenticationComponent,
    data: {
      routeName: 'authentication'
    }
  },
  {
    path: 'signup',
    data: {
      routeName: 'signup'
    },
    component: SignUpComponent
  },
  {
    path: 'confirmation/:token',
    component: ConfirmationComponent,
    data: {
      routeName: 'confirmation'
    }
  },
  {
    path: 'panel',
    loadChildren: './panel/panel.module#PanelModule',
    data: {
      routeName: 'panel'
    },
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'examination/:uuid',
    loadChildren: './examination/examination.module#ExaminationModule',
    canActivate: [
      AuthGuard
    ]
  }
];
