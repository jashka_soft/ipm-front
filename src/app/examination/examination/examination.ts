import { Unit } from 'itpm-shared';

export enum ExaminationStatus {
  STARTED = 1,
  END = 2,
}

export enum ExaminationTab {
  EMPTY_TAB = 1,
  THEORY = 2,
  SCHEMA = 3,
  TEST = 4,
}

export interface Examination {
  id: number;
  current_tab: number;
  created_at: string;
  updated_at: string;
  status: number;
  unit_id: number;
  user_id: number;
  uuid: string;
  unit: Unit;
}