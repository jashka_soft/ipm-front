import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { Subscription } from "rxjs/Subscription";

import { ToastsManager } from "ng2-toastr";

import { Unit } from 'itpm-shared';

import { EX_DATA, ExaminationService } from "./examination.service";
import { ExaminationStatus, ExaminationTab, Examination } from "./examination";
import { HttpErrorResponse } from '@angular/common/http';

export class CustomUnit extends Unit {
  public examinations: Array<Examination>;
}

const MAX_EXAMINATIONS_OF_UNIT: number = 3;

@Component({
  selector: 'app-examination',
  templateUrl: './examination.component.html',
  styleUrls: ['./examination.component.css']
})
export class ExaminationComponent implements OnInit, OnDestroy {
  public unit: CustomUnit;
  public examination: Examination;
  public showTabs: boolean = false;

  private uuid: string;
  private examinationTabStatuses: typeof ExaminationTab;
  private examinationStatuses: typeof ExaminationStatus;

  constructor(private ActivatedRoute: ActivatedRoute,
              private Router: Router,
              private ExaminationService: ExaminationService,
              private ToastsManager: ToastsManager) {
    this.examinationTabStatuses = ExaminationTab;
    this.examinationStatuses = ExaminationStatus;

    localStorage.setItem('warning', 'Изменяя тут что либо вы подвергаете себя при прохождении теста потерять информацию');
  }

  public ngOnInit() {
    this.uuid = this.ActivatedRoute.snapshot.params['uuid'];
    this.ExaminationService.load(this.uuid).subscribe((data: EX_DATA) => {
      this.examination = data.examination;
      this.unit = data.unit;

      if (this.unit.examinations.length === MAX_EXAMINATIONS_OF_UNIT) {
        this.ToastsManager.info(`Вы уже прошли этот юнит ${this.unit.examinations.length} раза.`);
        this.Router.navigateByUrl('/panel/');
      } else {
        if (this.examination.current_tab !== ExaminationTab.EMPTY_TAB) {
          this.continuous();
        }
      }

      this.ExaminationService.onUpdateExaminationData().subscribe((ex: EX_DATA) => {
        if (ex.examination) {
          this.examination.current_tab = ex.examination.current_tab;
          this.examination.status = ex.examination.status;
        }
      });
    }, (e: HttpErrorResponse) => {
      this.Router.navigateByUrl('/panel');
    });
  }

  public onEndTime() {
    this.ToastsManager.info('Examination deleting...');
    this.ExaminationService.cancel(this.examination.uuid).subscribe(() => {
      this.ToastsManager.success('Examination deleted');
      this.Router.navigateByUrl(`/panel`);
    });
  }

  get isShow() {
    return this.examination.current_tab > ExaminationTab.EMPTY_TAB;
  }

  public continuous() {
    const url = `/examination/${this.uuid}/`;
    switch (this.examination.current_tab) {
      case ExaminationTab.THEORY:
        this.Router.navigateByUrl(`${url}theory`);
        break;

      case ExaminationTab.SCHEMA:
        this.Router.navigateByUrl(`${url}schema`);
        break;

      case ExaminationTab.TEST:
        this.Router.navigateByUrl(`${url}test`);
        break;

    }

    this.showTab();
  }

  public showTab() {
    this.showTabs = true;
  }

  public go(tab?: number) {
    if (tab !== this.examination.current_tab && this.examination.current_tab !== ExaminationTab.TEST) {
      this.ExaminationService.updateTab(tab ? tab : ExaminationTab.THEORY, this.uuid).subscribe((examination: Examination) => {
        this.examination = examination;
        this.continuous();
      });
    }
  }

  public ngOnDestroy() {
    localStorage.removeItem('warning');
  }

}
