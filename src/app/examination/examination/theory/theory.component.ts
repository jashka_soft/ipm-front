import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { TheoryService } from "./theory.service";
import { ExaminationService } from "../examination.service";
import { ExaminationTab, Examination } from "../examination";
import { Theory } from "itpm-shared";

@Component({
  selector: 'ex-theory',
  templateUrl: './theory.component.html'
})
export class TheoryComponent implements OnInit {

  @ViewChild('slideshare') slidershare: ElementRef;

  public theory: Theory;
  public uuid: string;

  constructor(private TheoryService: TheoryService,
              private Router: Router,
              private ExaminationService: ExaminationService,
              private ActivatedRoute: ActivatedRoute) {
  }

  public ngOnInit() {
    this.uuid = this.ActivatedRoute.snapshot.parent.params['uuid'];
    this.TheoryService.get(this.uuid).subscribe((theory: Theory) => {
      this.theory = theory;
      if (theory && theory.iframe) {
        this.slidershare.nativeElement.innerHTML = theory.iframe;
      }
    });
  }

  public next() {
    this.ExaminationService.updateTab(ExaminationTab.SCHEMA, this.uuid).subscribe((examination: Examination) => {
      this.Router.navigate([`/examination/${this.uuid}/schema`]);
    });
  }
}