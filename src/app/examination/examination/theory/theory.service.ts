import { Injectable } from "@angular/core";

import "rxjs/operator/map";
import { Observable } from "rxjs";

import { AppHttpClient, Theory } from "itpm-shared";

@Injectable()
export class TheoryService {
  private prefix: string;

  constructor(private HttpClient: AppHttpClient) {
    this.prefix = 'examination/data/';
  }

  public get(uuid: string): Observable<Theory> {
    return this.HttpClient.get<Theory>(`${this.prefix}theory/${uuid}`);
  }
}