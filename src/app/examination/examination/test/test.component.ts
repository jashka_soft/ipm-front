import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TestService } from './test.service';
import { ExaminationService } from '../examination.service';
import { ExaminationStatus } from '../examination';

import { Answer, Test } from 'itpm-shared';

@Component({
  selector: 'ex-test',
  templateUrl: './test.component.html',
  styleUrls: [ `./test.component.css` ]
})
export class TestComponent {

  public test: Test;

  private uuid: string = null;

  constructor(private TestService: TestService,
              private ExaminationService: ExaminationService,
              private ActivatedRoute: ActivatedRoute,
              private Router: Router) {
    this.uuid = this.ActivatedRoute.snapshot.parent.params['uuid'];
    this.TestService.get(this.uuid).subscribe((test: Test) => {
      this.test = test;
    });
  }

  public setAnswer(answer: Answer, questionIndex: number) {
    this.test.questions[questionIndex].answer_id = answer.id;
  }

  public isSelected(questionAnswerId: number, answerId: number): boolean {
    return questionAnswerId === answerId;
  }

  public save(form) {
    if (form.valid) {
      this.TestService.save(this.test, this.test.id, this.uuid).subscribe(data => {
        this.ExaminationService.updateStatus(ExaminationStatus.END, this.uuid).subscribe(response => {
          this.Router.navigate(['/panel/']);
        });
      });
    }
  }
}