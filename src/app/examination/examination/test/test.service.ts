import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/operator/map";

import { AppHttpClient, Test } from 'itpm-shared';

@Injectable()
export class TestService {
  private prefix: string;

  constructor(private HttpClient: AppHttpClient) {
    this.prefix = 'examination/data/test/';
  }

  public get(uuid: string): Observable<Test> {
    return this.HttpClient.get<Test>(`${this.prefix}${uuid}`);
  }

  public save(test: Test, testId: number, uuid: string) {
    return this.HttpClient.post(`${this.prefix}${uuid}`, {
      questions: test.questions,
      test_id: testId,
      uuid
    });
  }
}