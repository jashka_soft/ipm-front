import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import "rxjs/operator/do";

import { AppHttpClient, Schema, Test } from 'itpm-shared';
import { Examination } from "./examination";
import { CustomUnit } from "./examination.component";

export interface EX_DATA {
  examination?: Examination;
  unit?: CustomUnit;
  schema?: Schema;
  test?: Test;
  counter?: number;
}

@Injectable()
export class ExaminationService {
  private prefix: string = 'examination/';
  private examination: Examination;
  private dataSource = new Subject<EX_DATA>();
  private dataSource$ = this.dataSource.asObservable();

  constructor(private HttpClient: AppHttpClient) {
  }

  public onUpdateExaminationData(): Observable<EX_DATA> {
    return this.dataSource$;
  }

  public getExamination(): Examination {
    return this.examination;
  }

  public cancel(uuid: string) {
    return this.HttpClient
      .delete(`${this.prefix}cancel/${uuid}`);
  }

  public load(uuid: string): Observable<EX_DATA> {
    return this.HttpClient
      .post<EX_DATA>(`${this.prefix}load`, { uuid })
      .do(this.doAction.bind(this));
  }

  public updateStatus(status: number, uuid: string) {
    return this.HttpClient
      .post(`${this.prefix}status`, { uuid, status })
      .do(this.doAction.bind(this));
  }

  public updateTab(current_tab: number, uuid: string): Observable<Examination> {
    return this.HttpClient
      .post<Examination>(`${this.prefix}tab`, { uuid, current_tab })
      .do((examination: Examination) => {
        this.doAction({ examination });
      });
  }

  private doAction(data: EX_DATA) {
    this.examination = data.examination;
    this.dataSource.next(data);
  }

}