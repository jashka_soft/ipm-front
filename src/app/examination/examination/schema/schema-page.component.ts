import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import "rxjs/operator/map";

import {
  AuthService,
  DiagramInstanceConfig,
  instances,
  Permissions,
  Schema,
  User,
  rc4
} from "itpm-shared";
import { SchemaPageService } from "./schema-page.service";
import { ExaminationService } from "../examination.service";
import { ExaminationTab, Examination } from "../examination";

@Component({
  selector: `ex-schema`,
  templateUrl: `./schema-page.component.html`,
  styleUrls: [`./schema-page.component.css`]
})
export class SchemaComponent {
  public types: DiagramInstanceConfig[] = [];
  public schema: Schema;
  public config: any = {};

  private originalSchema: Schema;
  private user: User;
  private uuid: string;

  constructor(private SchemaPageService: SchemaPageService,
              private ExaminationService: ExaminationService,
              private AuthService: AuthService,
              private Router: Router,
              private ActivatedRoute: ActivatedRoute,) {
    this.user = this.AuthService.getUser();
    this.types = instances;
    this.config.role = this.user.role;
    this.config.permission = Permissions.EXAMINATION;
    this.config.controls = false;
  }

  public ngOnInit() {
    this.uuid = this.ActivatedRoute.snapshot.parent.params['uuid'];
    this.SchemaPageService.get(this.uuid).subscribe((schema: Schema) => {
      this.schema = schema;
      this.originalSchema = schema;
    });
  }

  public save(newSchema: Schema) {
    let schema = JSON.parse(JSON.stringify(this.schema));
    schema.diagram = newSchema.diagram;
    this.schema = schema;
    if (schema.type === 'WBSOBS') {
      schema = this.SchemaPageService.checkAnswers(schema, this.originalSchema);
      console.log('prepare', schema.diagram)
    }
    schema.diagram = rc4(this.user.id.toString(), JSON.stringify(schema.diagram));
    console.log(schema.diagram);
    this.SchemaPageService.save(schema, this.schema.id, this.uuid).subscribe(() => {
      this.ExaminationService.updateTab(ExaminationTab.TEST, this.uuid).subscribe((examination: Examination) => {
        this.Router.navigateByUrl(`/examination/${this.uuid}/test`);
      });
    });
  }

}