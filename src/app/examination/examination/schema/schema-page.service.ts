import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/operator/map';

import { AppHttpClient, AuthService, rc4, Schema } from 'itpm-shared';

@Injectable()
export class SchemaPageService {
  private prefix: string;

  constructor(private HttpClient: AppHttpClient,
              private AuthService: AuthService) {
    this.prefix = 'examination/data/schema/';
  }

  public checkAnswers(schema: Schema, original: Schema) {
    const diagram = schema.diagram;
    const nodeDataArray = <Array<any>>diagram.nodeDataArray;
    for (let i = 0; i < nodeDataArray.length; ++i) {
      const node = nodeDataArray[i];
      if (node.hasOwnProperty('dropped')) {
        if (node['dropped_data']['key'] === node['key']) {
          nodeDataArray[i]['result'] = {result: true};
        } else {
          nodeDataArray[i]['result'] = {
            result: false,
            answer: this.getRightAnswer(original, node['key'])
          };
        }
      }
    }

    schema.diagram.nodeDataArray = nodeDataArray;

    return schema;
  }

  public get(uuid: string): Observable<Schema> {
    return this.HttpClient.get<Schema>(`${this.prefix}${uuid}`)
      .map((schema: Schema) => {
        const k = this.AuthService.getUser().id.toString();
        schema.diagram = JSON.parse(rc4(k, schema.diagram));
        return schema;
      });
  }

  public save({ diagram }: Schema, schemaId: number, uuid: string) {
    return this.HttpClient.post(`${this.prefix}${uuid}`, {
      schema: diagram,
      schema_id: schemaId,
      uuid
    });
  }

  private getRightAnswer(original: Schema, key: number) {
    const diagram = original.diagram;
    const nodeDataArray = <Array<any>>diagram.nodeDataArray;

    return nodeDataArray.find(node => node['key'] === key);
  }

}