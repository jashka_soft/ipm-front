import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { ExaminationService } from '../examination/examination.service';

@Injectable()
export class CanDeactivateExaminationGuard implements CanDeactivate<boolean> {
  constructor(private ExaminationService: ExaminationService) {
  }

  public canDeactivate(component: boolean,
                       currentRoute: ActivatedRouteSnapshot,
                       currentState: RouterStateSnapshot,
                       nextState?: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {


    return false;
  }
}