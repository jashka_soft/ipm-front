import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from "@angular/router";

import { ExaminationService } from "../examination/examination.service";
import { ExaminationTab, Examination } from "../examination/examination";

@Injectable()
export class CanActivateExaminationTabGuard implements CanActivate {
  constructor(private ExaminationService: ExaminationService) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const examination: Examination = this.ExaminationService.getExamination();

    if (examination) {

      switch (examination.current_tab) {
        case ExaminationTab.TEST:

          return route.data.tab === ExaminationTab.TEST;

        case ExaminationTab.SCHEMA:
        case ExaminationTab.THEORY:
        case ExaminationTab.EMPTY_TAB:

          return route.data.tab !== ExaminationTab.TEST;

      }

    }

    return false;
  }

}