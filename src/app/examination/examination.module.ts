import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { AuthModule, ITPMModule, NamedRouteModule, InterceptorMessageModule } from "itpm-shared";

import { ExaminationComponent } from "./examination/examination.component";
import { TheoryComponent } from "./examination/theory/theory.component";
import { TheoryService } from "./examination/theory/theory.service";
import { ExaminationService } from "./examination/examination.service";
import { SchemaComponent } from "./examination/schema/schema-page.component";
import { SchemaPageService } from "./examination/schema/schema-page.service";
import { TestService } from "./examination/test/test.service";
import { TestComponent } from "./examination/test/test.component";
import { CanActivateExaminationTabGuard } from "./guards/can-activate.guard";
import { routes } from "./routes";
import { environment } from "../../environments/environment";
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    FormsModule,

    LoadingBarHttpClientModule,
    ITPMModule.forRoot(environment),
    AuthModule,
    NamedRouteModule,
    InterceptorMessageModule,

    SharedModule,
  ],
  providers: [
    TheoryService,
    ExaminationService,
    SchemaPageService,
    TestService,

    CanActivateExaminationTabGuard
  ],
  declarations: [
    ExaminationComponent,
    TheoryComponent,
    SchemaComponent,
    TestComponent,
  ]
})
export class ExaminationModule {
}
