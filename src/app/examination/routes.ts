import { Routes } from "@angular/router";

import { CanActivateExaminationTabGuard } from "./guards/can-activate.guard";
import { TheoryComponent } from "./examination/theory/theory.component";
import { SchemaComponent } from "./examination/schema/schema-page.component";
import { ExaminationTab } from "./examination/examination";
import { ExaminationComponent } from "./examination/examination.component";
import { TestComponent } from "./examination/test/test.component";
// import { CanDeactivateExaminationGuard } from './guards/can-deactivate.guard';

export const routes: Routes = [
  {
    path: '',
    component: ExaminationComponent,
    // canDeactivate: [ CanDeactivateExaminationGuard ],
    children: [
      {
        path: 'theory',
        component: TheoryComponent,
        canActivate: [CanActivateExaminationTabGuard],
        data: {
          tab: ExaminationTab.THEORY
        }
      },
      {
        path: 'schema',
        component: SchemaComponent,
        canActivate: [CanActivateExaminationTabGuard],
        data: {
          tab: ExaminationTab.SCHEMA
        }
      },
      {
        path: 'test',
        component: TestComponent,
        canActivate: [CanActivateExaminationTabGuard],
        data: {
          tab: ExaminationTab.TEST
        }
      },
    ]
  }
];
