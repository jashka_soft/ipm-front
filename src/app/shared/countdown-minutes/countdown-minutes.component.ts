import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { interval } from 'rxjs/observable/interval';
import { map, share, timeInterval } from 'rxjs/operators';

@Component({
  selector: 'countdown-minutes',
  templateUrl: './countdown-minutes.component.html'
})
export class CountdownMinutesComponent implements OnInit, OnDestroy {
  @Input() start: string;
  @Input() maxMinutes: number = 20;

  @Output() onEnd: EventEmitter<boolean> = new EventEmitter<boolean>();

  public minutes: number = 0;

  private countdownSubscription: Subscription = null;

  public ngOnInit() {
    this.checkTime(this.start);
  }

  private checkTime(createdAt: string) {
    const minutes = this.getMinutes(createdAt);

    if (minutes >= this.maxMinutes) {
      this.onEndTime();
    } else {
      this.minutes = this.maxMinutes - minutes;
      const source: Observable<number> = interval(1000)
        .pipe(
          timeInterval(),
          map(() => this.getMinutes(createdAt)),
          share()
        );

      this.countdownSubscription = source.subscribe((minutes: number) => {
        if (minutes === this.maxMinutes) {
          this.onEndTime();
        } else {
          this.minutes = this.maxMinutes - minutes;
        }
      });
    }
  }

  public ngOnDestroy() {
    this.destroyCountdown();
  }

  private onEndTime() {
    this.destroyCountdown();
    this.onEnd.emit(true);
  }

  private destroyCountdown() {
    if (this.countdownSubscription) {
      this.countdownSubscription.unsubscribe();
    }
    this.minutes = 0;
  }

  private getMinutes(createdAt: string): number {
    const parsed: number = Date.parse(createdAt);
    const date = new Date(parsed);
    const today = new Date();
    const diff = today.getTime() - date.getTime();
    return Math.round(diff / 60000);
  }

}