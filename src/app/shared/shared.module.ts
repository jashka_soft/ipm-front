import { NgModule } from '@angular/core';

import { CountdownMinutesComponent } from './countdown-minutes/countdown-minutes.component';
import { ITPMModule } from 'itpm-shared';
import { environment } from '../../environments/environment';
import { CountdownServerService } from './countdown-server.service';

@NgModule({
  imports: [
    ITPMModule.forRoot(environment),
  ],
  declarations: [
    CountdownMinutesComponent,
  ],
  exports: [
    CountdownMinutesComponent,
  ],
  providers: [
    CountdownServerService,
  ]
})
export class SharedModule {

}