import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AppHttpClient } from 'itpm-shared';

@Injectable()
export class CountdownServerService {
  public constructor(private HttpClient: AppHttpClient) {}

  public removeIfExpired(): Observable<any> {
    return this.HttpClient.get('examination/expires');
  }
}