import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { AuthService } from "itpm-shared";

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  public SignIn: FormGroup;

  constructor(private AuthService: AuthService,
              private router: Router,
              private fb: FormBuilder) {
    this.SignIn = this.fb.group({
      'email': [null, Validators.compose([
        Validators.email,
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required,
      ])]
    });
  }

  public ngOnInit() {
    if (this.AuthService.isAuth()) {
      this.router.navigate(['/panel']);
    }
  }

  public onSubmit(user) {
    this.AuthService.auth(user).subscribe(data => {
      this.router.navigate(['/panel']);
    });
  }

}
