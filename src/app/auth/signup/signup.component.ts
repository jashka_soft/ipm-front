import { Component } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthService, User } from "itpm-shared";

function passwordConfirming(control: AbstractControl): any {
  if (!control.parent || !control) return;
  const pwd = control.parent.get('password');
  const cpwd = control.parent.get('confirm_password');

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return {invalid: true};

  }
}

@Component({
  selector: 'signup',
  templateUrl: `./signup.component.html`
})
export class SignUpComponent {

  public SignUp: FormGroup;
  public saved: boolean = false;
  public confirmationText: string = 'На ваший email було відправлено повідомлення з посиланням для підтвердження акаунту';

  constructor(private AuthService: AuthService,
              private router: Router,
              private fb: FormBuilder) {
    this.SignUp = fb.group({
      'name': [null, Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])],
      'email': [null, Validators.compose([
        Validators.email,
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required,
        Validators.minLength(8)
      ])],
      'password_confirmation': [null, Validators.compose([
        Validators.required,
        Validators.minLength(8),
        passwordConfirming
      ])]
    });
  }

  public ngOnInit() {
    if (this.AuthService.isAuth()) {
      this.router.navigate(['/panel']);
    }
  }

  public onSubmit(user: User) {
    this.AuthService.create(user).subscribe(data => {
      this.saved = true;
    });
  }
}