import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { Subscription } from "rxjs/Subscription";

import { ConfirmationService } from "./confirmation.service";
import { AuthService } from "itpm-shared";

@Component({
  selector: 'user-confirmation',
  template: ``
})
export class ConfirmationComponent implements OnInit, OnDestroy {

  private sub: Subscription;

  constructor(private ConfirmationService: ConfirmationService,
              private AuthService: AuthService,
              private route: ActivatedRoute,
              private router: Router,) {
  }

  public ngOnInit(): void {
    if (this.AuthService.isAuth()) {
      this.router.navigate(['/panel']);
    }

    this.sub = this.route.params.subscribe(params => {
      this.ConfirmationService.confirmation(params['token']).subscribe((data: any) => {
        this.router.navigate(['/']);
      });
    })
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }

}