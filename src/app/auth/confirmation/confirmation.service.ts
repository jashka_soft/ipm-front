import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/operator/map";

import { environment } from "../../../environments/environment";
import { AppHttpClient } from "itpm-shared";

@Injectable()
export class ConfirmationService {
  private apiUrl: string;

  constructor(private AppHttpClient: AppHttpClient,) {
    this.apiUrl = environment.apiHost;
  }

  public confirmation(token: string): Observable<any> {
    return this.AppHttpClient
      .withoutPrefixUrl()
      .post<any>(`auth/confirmation`, {
        token: token
      });
  }

}