
export const environment = {
  production: true,
  hmr: undefined,
  apiHost: 'https://itpm-backend.cf/',
  apiPrefix: 'api/',
  adminPrefix: 'user/',
  attachments: 'https://s3-us-west-2.amazonaws.com/elasticbeanstalk-us-west-2-959741697641/'
};
