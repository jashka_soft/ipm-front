
export const environment = {
  production: false,
  hmr: false,
  apiHost: 'http://localhost:8000/',
  apiPrefix: 'api/',
  adminPrefix: 'user/',
  attachments: 'https://s3-us-west-2.amazonaws.com/elasticbeanstalk-us-west-2-959741697641/'
};
